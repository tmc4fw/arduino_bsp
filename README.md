## Getting Started

This repo is available as a package usable with [Arduino Boards Manager](https://www.arduino.cc/en/guide/cores).

Add this link in the "*Additional Boards Managers URLs*" field:
- Open bitbucket https://bitbucket.org/tmc4fw/ardu_manager/src/master/package_atcontrols_index.json
- Select OpenRaw for file `package_atcontrols_index.json`
- Copy the URL from web-browser and paste to "*Additional Boards Managers URLs*" such as: 
https://bitbucket.org/tmc4fw/ardu_manager/raw/a206f6e33bf1fa65553315e9c192f5db6b8330b5/package_atcontrols_index.json

## Supported boards

 - TMC4 with L496Q.
 - TMC4 with L4R5Q.
