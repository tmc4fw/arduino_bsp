#ifndef __AD5592_DRIVER_H__
#define __AD5592_DRIVER_H__

#include <Arduino.h>
#include <SPI.h>

#define CH_MODE_UNUSED				0
#define CH_MODE_ADC						1
#define CH_MODE_DAC						2
#define CH_MODE_DAC_AND_ADC		3
#define CH_MODE_GPI						4
#define CH_MODE_GPO						5

#define CH_OFFSTATE_PULLDOWN			0
#define CH_OFFSTATE_OUT_LOW				1
#define CH_OFFSTATE_OUT_HIGH			2
#define CH_OFFSTATE_OUT_TRISTATE	3

#define BIT(nr) (1UL << (nr))

#define AD5592R_GPIO_READBACK_EN	BIT(10)
#define AD5592R_LDAC_READBACK_EN	BIT(6)

#ifndef swab16
#define swab16(x) \
	((((x) & 0x00ff) << 8) | \
	 (((x) & 0xff00) >> 8))
#endif

enum ad5592r_registers {
	AD5592R_REG_NOOP					= 0x0,
	AD5592R_REG_DAC_READBACK	= 0x1,
	AD5592R_REG_ADC_SEQ				= 0x2,
	AD5592R_REG_CTRL					= 0x3,
	AD5592R_REG_ADC_EN				= 0x4,
	AD5592R_REG_DAC_EN				= 0x5,
	AD5592R_REG_PULLDOWN			= 0x6,
	AD5592R_REG_LDAC					= 0x7,
	AD5592R_REG_GPIO_OUT_EN		= 0x8,
	AD5592R_REG_GPIO_SET			= 0x9,
	AD5592R_REG_GPIO_IN_EN		= 0xA,
	AD5592R_REG_PD						= 0xB,
	AD5592R_REG_OPEN_DRAIN		= 0xC,
	AD5592R_REG_TRISTATE			= 0xD,
	AD5592R_REG_RESET					= 0xF,
};

#define AD5592R_REG_PD_PD_ALL			    			BIT(10)
#define AD5592R_REG_PD_EN_REF			    			BIT(9)

#define AD5592R_REG_CTRL_ADC_PC_BUFF		    BIT(9)
#define AD5592R_REG_CTRL_ADC_BUFF_EN		    BIT(8)
#define AD5592R_REG_CTRL_CONFIG_LOCK		    BIT(7)
#define AD5592R_REG_CTRL_W_ALL_DACS		    	BIT(6)
#define AD5592R_REG_CTRL_ADC_RANGE		    	BIT(5)
#define AD5592R_REG_CTRL_DAC_RANGE		    	BIT(4)

#define AD5592R_REG_ADC_SEQ_REP			    		BIT(9)
#define AD5592R_REG_ADC_SEQ_TEMP_READBACK	  BIT(8)
#define AD5592R_REG_ADC_SEQ_CODE_MSK(x)		  ((x) & 0x0FFF)

#define AD5592R_REG_GPIO_OUT_EN_ADC_NOT_BUSY	BIT(8)

#define AD5592R_REG_LDAC_IMMEDIATE_OUT		    0x00
#define AD5592R_REG_LDAC_INPUT_REG_ONLY		    0x01
#define AD5592R_REG_LDAC_INPUT_REG_OUT		    0x02

#define INTERNAL_VREF_VOLTAGE			    2.5

struct ad5592r_dev;

/*
* Driver class.
*/
class AD5592 {
private:
  SPIClass *_spi; /*! This points to a valid SPI object created from the Arduino SPI library. */
public:
  AD5592(SPIClass *spi, void(*cs_sel)(void), void(*cs_release)(void));
  AD5592(SPIClass *spi);
  ~AD5592();
  void ad5592r_cs_cbfunc(void(*cs_sel)(void), void(*cs_release)(void));
  /**
   * Initialize AD5593r device.
   *
   * @param int_ref - The initial parameters of the device.
   * @return 0 in case of success, negative error code otherwise
   */
  int32_t ad5592r_init(bool int_ref);

  /**
   * Get GPIO value
   *
   * @param channel - The channel number.
   * @return 0 or 1 depending on the GPIO value.
   */
  int32_t ad5592r_gpio_get(uint8_t channel);

  /**
   * Set GPIO value
   *
   * @param dev - The device structure.
   * @param channel - The channel number.
   * @param value - the GPIO value (0 or 1)
   */
  int32_t ad5592r_gpio_set(uint8_t channel, int32_t value);

  /**
   * Set GPIO as input
   *
   * @param dev - The device structure.
   * @param channel - The channel number.
   * @return 0 in case of success, negative error code otherwise
   */
  int32_t ad5592r_gpio_direction_input(uint8_t channel);

  /**
   * Set GPIO as output
   *
   * @param channel - The channel number.
   * @param value - GPIO value to set.
   * @return 0 in case of success, negative error code otherwise
   */
  int32_t ad5592r_gpio_direction_output(uint8_t channel, int32_t value);

  /**
   * Software reset device.
   *
   * @param dev - The device structure.
   * @param None
   * @return 0 in case of success, negative error code otherwise
   */
  int32_t ad5592r_software_reset(void);

  /**
   * Set channels modes.
   *
   * @param dev - The device structure.
   * @param None
   * @return 0 in case of success, negative error code otherwise
   */
  int32_t ad5592r_set_channel_modes(void);

  /**
   * @brief Reset channels and set GPIO to unused.
   *
   * @param None
   * @return 0 in case of success, negative error code otherwise
   */
  int32_t ad5592r_reset_channel_modes(void);

  /**
   * Write DAC channel.
   *
   * @param chan - The channel number.
   * @param value - DAC value
   * @return 0 in case of success, negative error code otherwise
   */
  int32_t ad5592r_write_dac(uint8_t chan, uint16_t value);

  /**
   * Read ADC channel.
   *
   * @param chan - The channel number.
   * @param value - ADC value
   * @return 0 in case of success, negative error code otherwise
   */
  int32_t ad5592r_read_adc(uint8_t chan, uint16_t *value);

  /**
   * Read Multiple ADC Channels.
   *
   * @param chans - The ADC channels to be readback
   * @param values - ADC value array
   * @return 0 in case of success, negative error code otherwise
   */
  int32_t ad5592r_multi_read_adc(uint16_t chans, uint16_t *value);

protected:
  int32_t spi_write_and_read(uint8_t *buffer, size_t len);

  int32_t spi_write(uint8_t * buffer, size_t len);

  int32_t ad5592r_reg_write(struct ad5592r_dev *dev, uint8_t reg, uint16_t value);

  int32_t ad5592r_spi_wnop_r16(struct ad5592r_dev *dev, uint16_t *buf);

  int32_t ad5592r_reg_read(struct ad5592r_dev *dev, uint8_t reg, uint16_t *value);
  
  int32_t ad5592r_gpio_read(struct ad5592r_dev *dev, uint8_t *value);

  uint8_t hweight8(uint8_t w);

  int32_t ad5592r_base_reg_write(struct ad5592r_dev *dev, uint8_t reg,
							   uint16_t value);

  int32_t ad5592r_base_reg_read(struct ad5592r_dev *dev, uint8_t reg,
							  uint16_t *value);
#if defined(SPI_HAS_TRANSACTION)
  // Allow sub-class to change
  SPISettings spiSettings;
#endif
};

#endif   // __AD5592_DRIVER_H__