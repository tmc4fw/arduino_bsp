#include <Arduino.h>
#include "AD5592.h"

struct ad5592r_dev
{
	uint16_t spi_msg;
	uint8_t num_channels;
	uint16_t cached_dac[8];
	uint16_t cached_gp_ctrl;
	uint8_t channel_modes[8];
	uint8_t channel_offstate[8];
	uint8_t gpio_out;
	uint8_t gpio_in;
	uint8_t gpio_val;
	uint8_t ldac_mode;
};

typedef struct
{
  void (*_select)  (void);
  void (*_release)(void);
} ad5592r_cs_t;

static struct ad5592r_dev m_ad5592r_dev;
static struct ad5592r_dev *dev;
static ad5592r_cs_t ad5592r_cs;

#if defined(SPI_HAS_TRANSACTION)
#define SPI_TRANSACTION_START _spi->beginTransaction(spiSettings) ///< Pre-SPI
#define SPI_TRANSACTION_END _spi->endTransaction()				  ///< Post-SPI
#else															  // SPI transactions likewise not present in older Arduino SPI lib
#define SPI_TRANSACTION_START									  ///< Dummy stand-in define
#define SPI_TRANSACTION_END										  ///< keeps compiler happy
#endif

int32_t AD5592::spi_write_and_read(uint8_t *buffer, size_t len)
{
	uint8_t rxbuf[3] = {0x00};
	uint8_t txbuf[3] = {0x00};
	uint16_t output = 0x00;
	memcpy(txbuf, buffer, 2);
	ad5592r_cs._select();
	SPI_TRANSACTION_START;
	_spi->transfer(txbuf, rxbuf, sizeof(txbuf));
	SPI_TRANSACTION_END;
	ad5592r_cs._release();
	output = (((rxbuf[0] << 8) | rxbuf[1]) << 1) | (rxbuf[2] && 0x80);
	output = swab16(output);
	memcpy(buffer, &output, 2);
	return 0;
}

int32_t AD5592::spi_write(uint8_t *buffer, size_t len)
{
	ad5592r_cs._select();
	SPI_TRANSACTION_START;
	_spi->transfer(buffer, len);
	SPI_TRANSACTION_END;
	ad5592r_cs._release();
	return 0;
}

/**
 * Write register.
 *
 * @param dev - The device structure.
 * @param reg - The register address.
 * @param value - register value
 * @return 0 in case of success, negative error code otherwise
 */
int32_t AD5592::ad5592r_reg_write(struct ad5592r_dev *dev, uint8_t reg, uint16_t value)
{
	if (!dev)
		return -1;

	dev->spi_msg = swab16((reg << 11) | value);

	return spi_write((uint8_t *)&dev->spi_msg,
					 sizeof(dev->spi_msg));
}

/**
 * Write NOP and read value.
 *
 * @param dev - The device structure.
 * @param buf - buffer where to read
 * @return 0 in case of success, negative error code otherwise
 */
int32_t AD5592::ad5592r_spi_wnop_r16(struct ad5592r_dev *dev, uint16_t *buf)
{
	int32_t ret;
	uint16_t spi_msg_nop = 0; /* NOP */

	ret = spi_write_and_read((uint8_t *)&spi_msg_nop, sizeof(spi_msg_nop));
	if (ret < 0)
		return ret;

	*buf = swab16(spi_msg_nop);

	return ret;
}

/**
 * Read register.
 *
 * @param dev - The device structure.
 * @param reg - The register address.
 * @param value - register value
 * @return 0 in case of success, negative error code otherwise
 */
int32_t AD5592::ad5592r_reg_read(struct ad5592r_dev *dev, uint8_t reg, uint16_t *value)
{
	int32_t ret;

	if (!dev)
		return -1;

	dev->spi_msg = swab16((AD5592R_REG_LDAC << 11) |
						  AD5592R_LDAC_READBACK_EN | (reg << 2) | dev->ldac_mode);

	ret = spi_write((uint8_t *)&dev->spi_msg,
					sizeof(dev->spi_msg));
	if (ret < 0)
		return ret;

	ret = ad5592r_spi_wnop_r16(dev, &dev->spi_msg);
	if (ret < 0)
		return ret;

	*value = dev->spi_msg;

	return 0;
}

/**
 * Read GPIOs.
 *
 * @param dev - The device structure.
 * @param value - GPIOs value.
 * @return 0 in case of success, negative error code otherwise
 */
int32_t AD5592::ad5592r_gpio_read(struct ad5592r_dev *dev, uint8_t *value)
{
	int32_t ret;

	if (!dev)
		return -1;

	ret = ad5592r_reg_write(dev, AD5592R_REG_GPIO_IN_EN,
							AD5592R_GPIO_READBACK_EN | dev->gpio_in);
	if (ret < 0)
		return ret;

	ret = ad5592r_spi_wnop_r16(dev, &dev->spi_msg);
	if (ret < 0)
		return ret;

	*value = (uint8_t)dev->spi_msg;

	return 0;
}

AD5592::AD5592(SPIClass *spi, void(*cs_sel)(void), void(*cs_release)(void))
{
	_spi = spi;
	ad5592r_cs._select = cs_sel;
	ad5592r_cs._release = cs_release;
}

AD5592::AD5592(SPIClass *spi)
{
	_spi = spi;
}

AD5592::~AD5592() 
{

}

void AD5592::ad5592r_cs_cbfunc(void(*cs_sel)(void), void(*cs_release)(void))
{
	ad5592r_cs._select = cs_sel;
	ad5592r_cs._release = cs_release;
}

/**
 * Initialize AD5593r device.
 *
 * @param init_param - The initial parameters of the device.
 * @return 0 in case of success, negative error code otherwise
 */
int32_t AD5592::ad5592r_init(bool int_ref)
{
	int32_t ret = 0;
	uint16_t temp_reg_val;

	dev = &m_ad5592r_dev;

	dev->num_channels = 8;
	dev->channel_modes[0] = CH_MODE_ADC;
	dev->channel_modes[1] = CH_MODE_ADC;
	dev->channel_modes[2] = CH_MODE_GPI;
	dev->channel_modes[3] = CH_MODE_GPI;
	dev->channel_modes[4] = CH_MODE_GPI;
	dev->channel_modes[5] = CH_MODE_GPO;
	dev->channel_modes[6] = CH_MODE_GPO;
	dev->channel_modes[7] = CH_MODE_DAC;

	ret = ad5592r_software_reset();
	if (ret < 0)
		return ret;

	ret = ad5592r_set_channel_modes();
	if (ret < 0)
		return ret;

	if (int_ref)
	{
		ret = ad5592r_reg_read(dev, AD5592R_REG_PD, &temp_reg_val);
		if (ret < 0)
			return ret;
		temp_reg_val |= AD5592R_REG_PD_EN_REF;

		return ad5592r_reg_write(dev, AD5592R_REG_PD, temp_reg_val);
	}

	return ret;
}

/**
 * Write register.
 *
 * @param dev - The device structure.
 * @param reg - The register address.
 * @param value - register value
 * @return 0 in case of success, negative error code otherwise
 */
int32_t AD5592::ad5592r_base_reg_write(struct ad5592r_dev *dev, uint8_t reg,
							   uint16_t value)
{
	return ad5592r_reg_write(dev, reg, value);
}

/**
 * Read register.
 *
 * @param dev - The device structure.
 * @param reg - The register address.
 * @param value - register value
 * @return 0 in case of success, negative error code otherwise
 */
int32_t AD5592::ad5592r_base_reg_read(struct ad5592r_dev *dev, uint8_t reg,
							  uint16_t *value)
{
	return ad5592r_reg_read(dev, reg, value);
}

int32_t AD5592::ad5592r_gpio_get(uint8_t channel)
{
	int32_t ret = 0;
	uint8_t val;

	if (!dev)
		return -1;

	if (dev->gpio_out & BIT(channel))
		val = dev->gpio_val;
	else
		ret = ad5592r_gpio_read(dev, &val);

	if (ret < 0)
		return ret;

	ret = !!(val & BIT(channel));
	return ret;
}

int32_t AD5592::ad5592r_gpio_set(uint8_t channel, int32_t value)
{
	if (!dev)
		return -1;

	if (value)
		dev->gpio_val |= BIT(channel);
	else
		dev->gpio_val &= ~BIT(channel);

	int res = ad5592r_base_reg_write(dev, AD5592R_REG_GPIO_SET,
									 dev->gpio_val);

	return res;
}

int32_t AD5592::ad5592r_gpio_direction_input(uint8_t channel)
{
	int32_t ret;

	if (!dev)
		return -1;

	dev->gpio_out &= ~BIT(channel);
	dev->gpio_in |= BIT(channel);

	ret = ad5592r_base_reg_write(dev, AD5592R_REG_GPIO_OUT_EN,
								 dev->gpio_out);
	if (ret < 0)
		return ret;

	ret = ad5592r_base_reg_write(dev, AD5592R_REG_GPIO_IN_EN,
								 dev->gpio_in);

	return ret;
}

int32_t AD5592::ad5592r_gpio_direction_output(uint8_t channel, int32_t value)
{
	int32_t ret;

	if (!dev)
		return -1;

	if (value)
		dev->gpio_val |= BIT(channel);
	else
		dev->gpio_val &= ~BIT(channel);

	dev->gpio_in &= ~BIT(channel);
	dev->gpio_out |= BIT(channel);

	ret = ad5592r_base_reg_write(dev, AD5592R_REG_GPIO_SET, dev->gpio_val);
	if (ret < 0)
		return ret;

	ret = ad5592r_base_reg_write(dev, AD5592R_REG_GPIO_OUT_EN,
								 dev->gpio_out);
	if (ret < 0)
		return ret;

	ret = ad5592r_base_reg_write(dev, AD5592R_REG_GPIO_IN_EN,
								 dev->gpio_in);

	return ret;
}

int32_t AD5592::ad5592r_software_reset(void)
{
	int32_t ret;

	if (!dev)
		return -1;

	/* Writing this magic value resets the device */
	ret = ad5592r_base_reg_write(dev, AD5592R_REG_RESET, 0xdac);

	delay(10);

	return ret;
}

/**
 * Set channels modes.
 *
 * @param dev - The device structure.
 * @return 0 in case of success, negative error code otherwise
 */
int32_t AD5592::ad5592r_set_channel_modes(void)
{
	int32_t ret;
	uint8_t i;
	uint8_t pulldown = 0, tristate = 0, dac = 0, adc = 0;
	uint16_t read_back;

	if (!dev)
		return -1;

	dev->gpio_in = 0;
	dev->gpio_out = 0;

	for (i = 0; i < dev->num_channels; i++)
	{
		switch (dev->channel_modes[i])
		{
		case CH_MODE_DAC:
			dac |= BIT(i);
			break;

		case CH_MODE_ADC:
			adc |= BIT(i);
			break;

		case CH_MODE_DAC_AND_ADC:
			dac |= BIT(i);
			adc |= BIT(i);
			break;

		case CH_MODE_GPI:
			dev->gpio_in |= BIT(i);
			break;

		case CH_MODE_GPO:
			dev->gpio_out |= BIT(i);
			break;

		case CH_MODE_UNUSED:
		/* fall-through */
		default:
			switch (dev->channel_offstate[i])
			{
			case CH_OFFSTATE_OUT_TRISTATE:
				tristate |= BIT(i);
				break;

			case CH_OFFSTATE_OUT_LOW:
				dev->gpio_out |= BIT(i);
				break;

			case CH_OFFSTATE_OUT_HIGH:
				dev->gpio_out |= BIT(i);
				dev->gpio_val |= BIT(i);
				break;

			case CH_OFFSTATE_PULLDOWN:
			/* fall-through */
			default:
				pulldown |= BIT(i);
				break;
			}
		}
	}

	/* Pull down unused pins to GND */
	ret = ad5592r_base_reg_write(dev, AD5592R_REG_PULLDOWN, pulldown);
	if (ret < 0)
		return ret;

	ret = ad5592r_base_reg_write(dev, AD5592R_REG_TRISTATE, tristate);
	if (ret < 0)
		return ret;

	/* Configure pins that we use */
	ret = ad5592r_base_reg_write(dev, AD5592R_REG_DAC_EN, dac);
	if (ret < 0)
		return ret;

	ret = ad5592r_base_reg_write(dev, AD5592R_REG_ADC_EN, adc);
	if (ret < 0)
		return ret;

	ret = ad5592r_base_reg_write(dev, AD5592R_REG_GPIO_SET, dev->gpio_val);
	if (ret < 0)
		return ret;

	ret = ad5592r_base_reg_write(dev, AD5592R_REG_GPIO_OUT_EN,
								 dev->gpio_out);
	if (ret < 0)
		return ret;

	ret = ad5592r_base_reg_write(dev, AD5592R_REG_GPIO_IN_EN,
								 dev->gpio_in);
	if (ret < 0)
		return ret;

	/* Verify that we can read back at least one register */
	ret = ad5592r_base_reg_read(dev, AD5592R_REG_GPIO_IN_EN, &read_back);
	ret = ad5592r_base_reg_read(dev, AD5592R_REG_GPIO_OUT_EN, &read_back);
	ret = ad5592r_base_reg_read(dev, AD5592R_REG_GPIO_SET, &read_back);
	ret = ad5592r_base_reg_read(dev, AD5592R_REG_DAC_EN, &read_back);
	ret = ad5592r_base_reg_read(dev, AD5592R_REG_ADC_EN, &read_back);
	if (!ret && (read_back & 0xff) != adc)
		return -1;

	return ret;
}

int32_t AD5592::ad5592r_reset_channel_modes(void)
{
	uint32_t i;

	if (!dev)
		return -1;

	for (i = 0; i < sizeof(dev->channel_modes); i++)
		dev->channel_modes[i] = CH_MODE_UNUSED;

	return ad5592r_set_channel_modes();
}

/**
 * Write DAC channel.
 *
 * @param dev - The device structure.
 * @param chan - The channel number.
 * @param value - DAC value
 * @return 0 in case of success, negative error code otherwise
 */
int32_t AD5592::ad5592r_write_dac(uint8_t chan, uint16_t value)
{
	if (!dev)
		return -1;

	dev->spi_msg = swab16(BIT(15) | (uint16_t)(chan << 12) | value);

	int res = spi_write((uint8_t *)&dev->spi_msg,
						sizeof(dev->spi_msg));

	return res;
}

/**
 * Read ADC channel.
 *
 * @param chan - The channel number.
 * @param value - ADC value
 * @return 0 in case of success, negative error code otherwise
 */
int32_t AD5592::ad5592r_read_adc(uint8_t chan, uint16_t *value)
{
	int32_t ret;

	if (!dev)
		return -1;

	dev->spi_msg = swab16((uint16_t)(AD5592R_REG_ADC_SEQ << 11) | BIT(chan));

	ret = spi_write((uint8_t *)&dev->spi_msg, sizeof(dev->spi_msg));
	if (ret < 0)
		return ret;

	/*
	 * Invalid data:
	 * See Figure 40. Single-Channel ADC Conversion Sequence
	 */
	ret = ad5592r_spi_wnop_r16(dev, &dev->spi_msg);
	if (ret < 0)
		return ret;

	ret = ad5592r_spi_wnop_r16(dev, &dev->spi_msg);
	if (ret < 0)
		return ret;

	*value = dev->spi_msg;

	return 0;
}

uint8_t AD5592::hweight8(uint8_t w)
{
	uint8_t res = w - ((w >> 1) & 0x55);
	res = (res & 0x33) + ((res >> 2) & 0x33);
	return (res + (res >> 4)) & 0x0F;
}

/**
 * Read Multiple ADC Channels.
 *
 * @param chans - The ADC channels to be readback
 * @param values - ADC value array
 * @return 0 in case of success, negative error code otherwise
 */
int32_t AD5592::ad5592r_multi_read_adc(uint16_t chans, uint16_t *values)
{
	int32_t ret;
	uint8_t samples;
	uint8_t i;

	if (!dev)
		return -1;

	samples = hweight8(chans);

	dev->spi_msg = swab16((uint16_t)(AD5592R_REG_ADC_SEQ << 11) | chans);

	ret = spi_write((uint8_t *)&dev->spi_msg,
					sizeof(dev->spi_msg));
	if (ret < 0)
		return ret;

	/*
	 * Invalid data:
	 * See Figure 40. Single-Channel ADC Conversion Sequence
	 */
	ret = ad5592r_spi_wnop_r16(dev, &dev->spi_msg);
	if (ret < 0)
		return ret;

	for (i = 0; i < samples; i++)
	{
		ret = ad5592r_spi_wnop_r16(dev, &dev->spi_msg);
		if (ret < 0)
			return ret;
		values[i] = dev->spi_msg;
	}

	return 0;
}