#include "SX1503.h"

/* Real register addresses */
static const uint8_t SX1503_REG_ADDR[31] = {
  0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
  0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
  0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 
  0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E
};

/*
* Constructor.
*/
SX1503::SX1503(TwoWire* _wire) {
    _bus = _wire;
}

/*
* Destructor.
*/
SX1503::~SX1503() {
}

void SX1503::begin() {
    registers[SX1503_REG_DATA_B] = 0xFF;
    registers[SX1503_REG_DATA_A] = 0xFF;
    registers[SX1503_REG_DIR_B] = 0xFF;
    registers[SX1503_REG_DIR_A] = 0xFF;
    registers[SX1503_REG_PULLUP_B] = 0x00;
    registers[SX1503_REG_PULLUP_A] = 0x00;
    registers[SX1503_REG_PULLDOWN_B] = 0x00;
    registers[SX1503_REG_PULLDOWN_A] = 0x00;
    registers[SX1503_REG_IRQ_MASK_B] = 0xFF;
    registers[SX1503_REG_IRQ_MASK_A] = 0xFF;

    _bus->begin();
}

int8_t SX1503::digitalWrite(uint8_t pin, uint8_t value) {
  int8_t ret = -2;
  if (pin < 16) {
    ret = 0;
    uint8_t reg0 = (pin < 8) ? SX1503_REG_DATA_A : SX1503_REG_DATA_B;
    uint8_t val0 = (pin < 8) ? registers[SX1503_REG_DATA_A] : registers[SX1503_REG_DATA_B];
    pin = pin & 0x07; // Restrict to 8-bits.
    uint8_t f = 1 << pin;
    val0 = (0 != value) ? (val0 | f) : (val0 & ~f);
    if ((0 == ret) & (registers[reg0] != val0)) {  ret = _write_register(reg0, val0);   }
  }
  return ret;
}

uint8_t SX1503::digitalRead(uint8_t pin) {
  uint8_t ret = 0;
  uint8_t reg = 0x00;
  if (pin < 8) {
    reg = SX1503_REG_DATA_A;
  } else if (pin < 16) {
    reg = SX1503_REG_DATA_B;
  }

  ret = _read_register(reg, 1);
  registers[reg] = ret;
  return ret & (1 << pin ) ? 1U : 0U;
}

uint8_t SX1503::readPort(uint8_t port) {
  uint8_t ret = 0;
  uint8_t reg = (port == SX1503_PORT_A ? SX1503_REG_DATA_A : SX1503_REG_DATA_B);
  ret = _read_register(reg, 1);
  registers[reg] = ret;
  return ret;
}

uint16_t SX1503::readPort() {
    uint8_t portB, portA = 0;
    portA = _read_register(SX1503_REG_DATA_A, 1);
    portB = _read_register(SX1503_REG_DATA_B, 1);
    registers[SX1503_REG_DATA_A] = portA;
    registers[SX1503_REG_DATA_B] = portB;
    return (portB << 8 | portA);
}

void SX1503::writePort(uint8_t port, uint8_t val)
{
  uint8_t ret = 0;
  uint8_t reg = (port == SX1503_PORT_A ? SX1503_REG_DATA_A : SX1503_REG_DATA_B);
  _write_register(reg, val);
}

void SX1503::writePort(uint16_t val)
{
    writePort(SX1503_PORT_A, (val & 0x00FF));
    writePort(SX1503_REG_DATA_B, ((val >> 8) & 0x00FF));
}

int8_t SX1503::pinMode(uint8_t pin, int mode) {
  uint8_t ret = -1;
  if (pin < 16) {
    ret = 0;
    bool in = true;
    bool pu = false;
    bool pd = false;
    switch (mode) {
      case OUTPUT_PULLDOWN:
        pd = true;
        in = false;
        break;
      case OUTPUT_PULLUP:
        pu = true;
        in = false;
        break;
      case OUTPUT:
        in = false;
        break;
      case INPUT_PULLUP:
        pu = true;
        break;
      case INPUT_PULLDOWN:
        pd = true;
        break;
      case INPUT:
      default:
        break;
    }

    uint8_t reg0 = (pin < 8) ? SX1503_REG_DIR_A : SX1503_REG_DIR_B;
    uint8_t reg1 = (pin < 8) ? SX1503_REG_PULLUP_A : SX1503_REG_PULLUP_B;
    uint8_t reg2 = (pin < 8) ? SX1503_REG_PULLDOWN_A : SX1503_REG_PULLDOWN_B;
    uint8_t reg3 = (pin < 8) ? SX1503_REG_IRQ_MASK_A : SX1503_REG_IRQ_MASK_B;
    uint8_t val0 = registers[reg0];
    uint8_t val1 = registers[reg1];
    uint8_t val2 = registers[reg2];
    uint8_t val3 = registers[reg3];
    pin = pin & 0x07; // Restrict to 8-bits.
    uint8_t f = 1 << pin;

    // Pin being set as an input means we need to unmask the interrupt.
    val0 = (in) ? (val0 | f) : (val0 & ~f);
    val1 = (pu) ? (val1 | f) : (val1 & ~f);
    val2 = (pd) ? (val2 | f) : (val2 & ~f);
    val3 = (in) ? (val3 & ~f) : (val3 | f);

    if ((0 == ret) & (registers[reg0] != val0)) {  ret = _write_register(reg0, val0);   }
    if ((0 == ret) & (registers[reg1] != val1)) {  ret = _write_register(reg1, val1);   }
    if ((0 == ret) & (registers[reg2] != val2)) {  ret = _write_register(reg2, val2);   }
    if ((0 == ret) & (registers[reg3] != val3)) {  ret = _write_register(reg3, val3);   }
  }
  return ret;
}

int8_t SX1503::_write_register(uint8_t reg, uint8_t val) {
  int8_t ret = -1;
  if (nullptr != _bus) {
    // No special safety measures are needed here.
    _bus->beginTransmission((uint8_t) SX1503_I2C_ADDR);
    _bus->write(SX1503_REG_ADDR[reg]);
    _bus->write(val);
    ret = _bus->endTransmission();
    if (0 == ret) {
      registers[reg] = val;
    }
  }
  return ret;
}

int8_t SX1503::_read_register(uint8_t reg, uint8_t len) {
  int8_t ret = -1;
  if (nullptr != _bus) {
    _bus->beginTransmission((uint8_t) SX1503_I2C_ADDR);
    _bus->write(SX1503_REG_ADDR[reg]);
    ret = (int8_t) _bus->endTransmission(false);
    if (0 == ret) {
      _bus->requestFrom((uint8_t) SX1503_I2C_ADDR, len);
      for (uint8_t i = 0; i < len; i++) {
        registers[reg + i] = _bus->read();
      }
      ret = _bus->endTransmission();
    }
  }
  return ret;
}
